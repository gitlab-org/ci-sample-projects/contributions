# ➕ Contributions

To add a project to the group, make a proposal by opening an issue in this project. Select the template `contribute_a_project`

## Anticipated Contributions

| Project idea | Comments | Added |
|--------------|----------|-------|
| Project with compliance pipelines |  |  |
| Showcase scenarios around artifact handling |  |  |
|   |  |  |


